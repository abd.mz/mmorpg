import java.util.Random;


public abstract class Personnage {

/*------------------------------------------ ATTRIBUTS ------------------------------------------*/
	private int vie;
	private int viemax;
	private String nom;
	private int typePers; // si = 1 --> PJ si =2 --> PNJ
	private int posx;
	private int posy;
	private int xp;
	private int xpmax;
	private int force;
	private int adresse;
	private int resistance;
	private int lvl;
	private int lvlmax;



/*------------------------------------------ CONSTRUCTEUR ------------------------------------------*/


	public Personnage() {}
	public Personnage(String nom , int vie, int viemax,int typePers, int posx, int posy,int force, int adresse, int resistance, int lvl, int lvlmax){
		this.setLvl(lvlmax);
		this.setLvlmax(lvlmax);
		this.setVie(vie);
		this.setViemax(viemax);
		this.setPosx(posx);
		this.setPosy(posy);
		this.setForce(force);
		this.setAdresse(adresse);
		this.setResistance(resistance);

	}
    public int getLvlmax() {
		return lvlmax;
	}
	public void setLvlmax(int lvlmax) {
		this.lvlmax = lvlmax;
	}
	public Personnage(int x, int y) {
        this.posx = x;
        this.posy = y;
        this.vie= 10;
        this.viemax = 10;
        this.force = -1;
        this.adresse = -1;
        this.resistance = -1;
    }

/*------------------------------------------ GET/SET ------------------------------------------*/

    public int getLvl() {
		return lvl;
	}
	public void setLvl(int lvl) {
		this.lvl = lvl;
	}
	public int getXp() {
		return xp;
	}
	public void setXp(int xp) {
		this.xp = xp;
	}
	public int getXpmax() {
		return xpmax;
	}
	public void setXpmax(int xpmax) {
		this.xpmax = xpmax;
	}
    public int getForce(){
		return this.force;
	}
	public void setForce(int force){
		this.force = force;
	}
	public int getAdresse(){
		return this.adresse;
	}
	public void setAdresse(int adresse){
		this.adresse = adresse;
	}
	public int getResistance(){
		return this.resistance;
	}
	public void setResistance(int resistance){
		this.resistance = resistance;
	}
	public int getPosx(){
		return this.posx;
	}
	public void setPosx(int posx){
		this.posx = posx;
	}
	public int getPosy(){
		return this.posy;
	}
	public void setPosy(int posy){
		this.posy=posy;
	}
	public int getTypePers(){
		return this.typePers;
	}
	public void setTypePers(int typePers){
		this.typePers= typePers;
	}
	public String getNom(){
		return this.nom;
	}
	public void setNom(String nom){
		this.nom = nom;
	}
	public int getVie(){
		return this.vie;
	}
	public void setVie(int vie){
		this.vie = vie;
	}
	public int getViemax(){
		return this.viemax;
	}
	public void setViemax(int viemax){
		this.viemax = viemax;
	}





/*------------------------------------------ METHODE ------------------------------------------*/
	public boolean estMort()
    {
  		if(this.vie <= 0)
      {
  			return true;
  		}
  		else
      {
  			return false;
  		}
  	}
	public int lancerDeDee()	{

	  Random rand= new Random();
	  int resultat_dee = 0;
	  for (int i = 0; i< this.getLvl(); i++)
	  {
	    resultat_dee += (rand.nextInt(6)+1);
	  }
		return resultat_dee;
	}


}
