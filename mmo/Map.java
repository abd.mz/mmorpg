import java.util.Random;
import java.util.Scanner;

public class Map {
    private Object[][] grille;

    public static int nbe; // Nombre Enemies


    public Map(int monstre)
    {
      this.grille = new Object[20][20];

      Map.nbe = monstre;

      Random rand = new Random();

      for (int i = 0; i<20 ; i++)
      {
        for (int j = 0; j<20; j++)
        {
            if(i == 0 || j == 0 || i == 19 || j == 19)
            {
              this.grille[i][j] = new Mur();
            }
            else
            {
              int  n = rand.nextInt(10);

              if(n == 0)
              {
                this.grille[i][j] = new Mur();
              }
              else
              {
                this.grille[i][j] = null;
              }
            }
        }
      }
      for (int i = 0; i<Map.nbe; i++)
      {
        int n = rand.nextInt(20);
        int m = rand.nextInt(20);
        if(this.estVide(n, m) && !(this.get_Case_Map(n-1,m) instanceof Mur))
        {
          this.set_Case_Map(n, m, new PNJ());
          System.out.println("n : "+n+"\nm : "+m);
        }
        else
        {
          i--;
        }
      }
    }

    public boolean estVide(int i, int j)
    {
      return(this.get_Case_Map(i, j) == null);
    }

    public void setnbe(int value)
    {
        Map.nbe = value;
    }

    public int getnbe()
    {
        return Map.nbe;
    }

    public void set_Case_Map(int x, int y, Object type)
    {
    	this.grille[x][y] = type;
    }

    public Object get_Case_Map(int x, int y)
    {
      return this.grille[x][y];
    }

    public int testCombat(Personnage p)
    {
      if(this.grille[p.getPosy()-1][p.getPosx()] instanceof Personnage)
      {
        return 1;
      }
      else if(this.grille[p.getPosy()+1][p.getPosx()] instanceof Personnage)
      {
        return 2;
      }
      else if(this.grille[p.getPosy()][p.getPosx()-1] instanceof Personnage)
      {
        return 3;
      }
      else if(this.grille[p.getPosy()][p.getPosx()+1] instanceof Personnage)
      {
        return 4;
      }
      else
      {
        return 0;
      }
    }

    public void passeTour(PJ pers)
    {
      pers.setPa(0);
    }

    public void testLoot(Personnage p)
    {
      if(this.grille[p.getPosy()-1][p.getPosx()] instanceof Objet)
      {
        Objet c = (Objet) this.grille[p.getPosy()-1][p.getPosx()];
        if(this.getLoot(c, (PJ)p))
        {
          this.set_Case_Map(p.getPosy()-1, p.getPosx(), null);
        }
      }
      else if(this.grille[p.getPosy()+1][p.getPosx()] instanceof Objet)
      {
        Objet c = (Objet) this.grille[p.getPosy()+1][p.getPosx()];
        if(this.getLoot(c, (PJ)p))
        {
          this.set_Case_Map(p.getPosy()+1, p.getPosx(), null);
        }
      }
      else if(this.grille[p.getPosy()][p.getPosx()-1] instanceof Objet)
      {
        Objet c = (Objet) this.grille[p.getPosy()][p.getPosx()-1];
        if(this.getLoot(c, (PJ)p))
        {
          this.set_Case_Map(p.getPosy(), p.getPosx()-1, null);
        }
      }
      else if(this.grille[p.getPosy()][p.getPosx()+1] instanceof Objet)
      {
        Objet c = (Objet) this.grille[p.getPosy()][p.getPosx()+1];
        if(this.getLoot(c, (PJ)p))
        {
          this.set_Case_Map(p.getPosy(), p.getPosx()+1, null);
        }
      }
    }

    public boolean getLoot(Objet e, PJ p)
    {
      boolean flag = false;
      if(e != null)
      {
        flag = p.prendreObjet(e);
      }
      else
      {
        System.out.println("Aucun objet à proximite");
      }
      return flag;
    }

    public boolean deplacerPJ(PJ p)
    {
      System.out.println("Tour de "+p.getNom());
      char c = getValAction();
      p.setPa(p.getPa()-1);
      if(c == 'a')
      {
        PNJ p1;
        int posE = this.testCombat(p);
        if(posE != 0)
        {
          boolean atkR;
          if(posE == 1)
          {
            p1 = (PNJ)this.grille[p.getPosy()-1][p.getPosx()];
          }
          else if(posE == 2)
          {
            p1 = (PNJ)this.grille[p.getPosy()+1][p.getPosx()];
          }
          else if(posE == 3)
          {
            p1 = (PNJ)this.grille[p.getPosy()][p.getPosx()-1];
          }
          else
          {
            p1 = (PNJ)this.grille[p.getPosy()][p.getPosx()+1];
          }
          atkR = p.attaquer(p1);
          if(atkR && p1.estMort())
          {
            this.set_Case_Map(p1.getPosy(), p1.getPosx(), null);
          }
        }
        else
        {
          System.out.println("Aucun ennemi à proximite !!!!");
          p.setPa(p.getPa()+1);
        }
      }

      else if(c == 'p')
      {
        this.passeTour(p);
      }

      else if(c == 'z' && this.grille[p.getPosy()-1][p.getPosx()] == null)
      {
        this.set_Case_Map(p.getPosy()-1, p.getPosx(), p);
        this.set_Case_Map(p.getPosy(), p.getPosx(), null);
        p.setPosy(p.getPosy()-1);
        System.out.println("a");
      }

      else if(c == 'q' && this.grille[p.getPosy()][p.getPosx()-1] == null)
      {
        this.set_Case_Map(p.getPosy(), p.getPosx()-1, p);
        this.set_Case_Map(p.getPosy(), p.getPosx(), null);
        p.setPosx(p.getPosx()-1);
        System.out.println("z");
      }
      else if(c == 'i') {
    	  System.out.println(p.getInv());
    	  Scanner sc = new Scanner(System.in);
          if(p.getInv().getNbo() > 0)
          {
            int indice;
            do {
              System.out.print("Veuillez inserer le numéro de l'equipement que vous souhaitez selectionner, sinon entrer 0 pour revenir dans la partie : ");
        			indice = sc.nextInt();
            } while (indice < 0 || indice > p.getInv().getNbo());

            if(indice != 0)
            {
              char action;

              do {
                System.out.print("j = Lacher, e = Equiper) : ");
                action = sc.next().charAt(0);
              } while (action != 'j' && action != 'e');

              if(action == 'j')
              {
                p.getInv().jeterObjet(indice-1);
              }
              else
              {
                if(p.getInv().getObjet(indice-1) instanceof Soin)
                {
                  p.utiliserPotionSoin((Soin)p.getInv().getObjet(indice-1));
                  p.getInv().jeterObjet(indice-1);
                  System.out.println("Vous avez consommé une potion !");
                  System.out.println("Votre vie = "+ p.getVie());
                }
                else if(p.getInv().getObjet(indice-1) instanceof Arme)
                {
                  Arme a = p.getArme();
                  p.setArme((Arme)p.getInv().getObjet(indice-1));
                  p.getInv().jeterObjet(indice-1);
                  p.getInv().ajouterObjet(a);
                }
                else
                {
                  Armure a = p.getArmure();
                  p.setArmure((Armure)p.getInv().getObjet(indice-1));
                  p.getInv().jeterObjet(indice-1);
                  p.getInv().ajouterObjet(a);
                }
              }
            }
          }
          else
          {
            System.out.println("Votre inventaire est vide \n(Enter)");
            sc.nextLine();
          }
          p.setPa(p.getPa()+1);
        }

      else if(c == 's' && this.grille[p.getPosy()+1][p.getPosx()] == null)
      {
        this.set_Case_Map(p.getPosy()+1, p.getPosx(), p);
        this.set_Case_Map(p.getPosy(), p.getPosx(), null);
        p.setPosy(p.getPosy()+1);
        System.out.println("e");
      }

      else if(c == 'd' && this.grille[p.getPosy()][p.getPosx()+1] == null)
      {
        this.set_Case_Map(p.getPosy(), p.getPosx()+1, p);
        this.set_Case_Map(p.getPosy(), p.getPosx(), null);
        p.setPosx(p.getPosx()+1);
        System.out.println("r");
      }
      else if(c == 'w')
      {
        testLoot(p);
      }
      else
      {
        p.setPa(p.getPa()+1);
      }
      if(p.getPa() == 0)
      {
        return false;
      }

      else
      {
        return true;
      }
    }

    public char getValAction()
    {
      char c;
      @SuppressWarnings("resource")
	Scanner sc = new Scanner(System.in);
      do
      {
        System.out.print("Action : z = HAUT, q = GAUCHE, s = BAS, d = DROITE, a = Attaquer, i = Inventaire, p = Passer un tour, w = Prendre un objet :   \n");
        c = sc.next().charAt(0);
      } while (c != 'z' && c != 'q' && c != 's' && c != 'd' && c != 'a' && c != 'i' && c != 'p' && c != 'w');
      return c;
    }

    public void deplacerPNJ(PNJ pnj, PJ pj)
    {
      int i = 0;
      while(i<3)
      {
        this.Afficher();
        if(this.testCombat(pnj)!=0)
        {
          int posE = this.testCombat(pj);
          if(posE != 0)
          {
            boolean atkR;
            atkR = pnj.attaquer(pj);
            if(atkR && pj.estMort())
            {
              this.set_Case_Map(pj.getPosy(), pj.getPosx(), null);
            }
          }
        }
        else if(pj.getPosx() < pnj.getPosx() && this.grille[pnj.getPosy()][pnj.getPosx()-1] == null)
        {
          this.set_Case_Map(pnj.getPosy(), pnj.getPosx()-1, pnj);
          this.set_Case_Map(pnj.getPosy(), pnj.getPosx(), null);
          pnj.setPosx(pnj.getPosx()-1);
        }
        else if(pj.getPosy() < pnj.getPosy() && this.grille[pnj.getPosy()-1][pnj.getPosx()] == null)
        {
          this.set_Case_Map(pnj.getPosy()-1, pnj.getPosx(), pnj);
          this.set_Case_Map(pnj.getPosy(), pnj.getPosx(), null);
          pnj.setPosy(pnj.getPosy()-1);
        }
        else if(pj.getPosx() > pnj.getPosx() && this.grille[pnj.getPosy()][pnj.getPosx()+1] == null)
        {
          this.set_Case_Map(pnj.getPosy(), pnj.getPosx()+1, pnj);
          this.set_Case_Map(pnj.getPosy(), pnj.getPosx(), null);
          pnj.setPosx(pnj.getPosx()+1);
        }
        else if(this.grille[pnj.getPosy()+1][pnj.getPosx()] == null)
        {
          this.set_Case_Map(pnj.getPosy()+1, pnj.getPosx(), pnj);
          this.set_Case_Map(pnj.getPosy(), pnj.getPosx(), null);
          pnj.setPosy(pnj.getPosy()+1);
        }
        else if(this.grille[pnj.getPosy()][pnj.getPosx()-1] == null)
        {
          this.set_Case_Map(pnj.getPosy(), pnj.getPosx()-1, pnj);
          this.set_Case_Map(pnj.getPosy(), pnj.getPosx(), null);
          pnj.setPosx(pnj.getPosx()-1);
        }
        else if(this.grille[pnj.getPosy()][pnj.getPosx()+1] == null)
        {
          this.set_Case_Map(pnj.getPosy(), pnj.getPosx()+1, pnj);
          this.set_Case_Map(pnj.getPosy(), pnj.getPosx(), null);
          pnj.setPosx(pnj.getPosx()+1);
        }
        else
        {
          this.set_Case_Map(pnj.getPosy()-1, pnj.getPosx(), pnj);
          this.set_Case_Map(pnj.getPosy(), pnj.getPosx(), null);
          pnj.setPosy(pnj.getPosy()-1);
        }

        i++;
      }
    }

    public void Afficher()
    {
      int i = 0;
      int j = 0;
      String ligne = new String("");
      for (i=0; i<20; i++)
      {
        for (j=0; j<20; j++)
        {
          if(get_Case_Map(i, j) instanceof Mur)
          {
            ligne+=("* ");
          }
          else if(get_Case_Map(i, j) instanceof PJ)
          {
            ligne+=("J ");
          }
          else if(get_Case_Map(i, j) instanceof PNJ)
          {
            ligne+=("M ");
          }
          else if(get_Case_Map(i, j) instanceof Potion)
          {
            ligne+=("S ");
          }
          else if(get_Case_Map(i, j) instanceof Arme)
          {
            ligne+=("A ");
          }
          else if(get_Case_Map(i, j) instanceof Armure)
          {
            ligne+=("B ");
          }
          else
          {
            ligne+=("  ");
          }
        }
        System.out.println(ligne);
        ligne="";



      }
    }
   }
