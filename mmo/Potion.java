public class Potion extends Objet {
 /*-------------------------------------- Attributs --------------------------------------------*/
    private int heal;

 /*-------------------------------------- CONSTRUCTEUR --------------------------------------------*/
    public Potion(String nom, int typeobjet, int quantite, int heal) {
        super(nom,typeobjet, quantite);
        this.heal = heal;
    }

 /*-------------------------------------- get/set --------------------------------------------*/
    public int getHeal() {
        return this.heal;
    }
}
