public abstract class Objet {
/*-------------------------------------- ATTRIBUTS --------------------------------------------*/
	private String nom;
	private int typeObjet; // si =1 -->  Arme si = 2 --> Armure si =3 --> Potion
	private int quantite;
	private int posy;
	private int posx;

/*-------------------------------------- CONSTRUCTEUR --------------------------------------------*/

	public Objet(){}
	public Objet(String nom, int typeObjet, int quantite){
		this.setNom(nom);
		this.setTypeObjet(typeObjet);
		this.setQuantite(quantite);
	}
/*-------------------------------------- GET/SET --------------------------------------------*/
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	public String getNom(){
		return this.nom;
	}
	public void setNom(String nom){
		this.nom = nom;
	}
	public int getTypeObjet(){
		return this.typeObjet;
	}
	public void setTypeObjet(int typeObjet){
		this.typeObjet= typeObjet;
	}
	public int getPosx() {
		return posx;
	}
	public void setPosx(int posx) {
		this.posx = posx;
	}
	public int getPosy() {
		return posy;
	}
	public void setPosy(int posy) {
		this.posy = posy;
	}
/*-------------------------------------- METHODE --------------------------------------------*/
	public String typeObjet() {
		if(this.typeObjet == 1) {
			return "Arme";
		}
		else if(this.typeObjet == 2) {
			return "Armure";
		}
		else {
			return "Potion";
		}
	}

}
