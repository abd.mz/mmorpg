import java.util.Scanner;

public class PJ extends Personnage {

/*----------------------------------------------- ATRIBUTS -------------------------------------*/
private int mana;
private int manamax;
private int pa;
private int pamax;
private int xp;
private int xpmax;
private Inventaire inv;
private Armure armure;
private Arme arme;
private Objet o;

/*----------------------------------------------- CONSTRUCTEUR -------------------------------------*/

public PJ()
{
  super();
	this.setLvl(0);
	this.setPa(0);
	this.setPamax(0);
	this.setXp(0);
	this.setVie(0);
}

public PJ(int x, int y)
{
  super(1, 1);
	this.setLvl(1);
	this.setPa(3);
	this.setPamax(3);
	this.setXp(0);
	this.inv = new Inventaire();
}

public PJ(String nom, int vie, int viemax,int posx, int posy,  int mana, int manamax, int force, int adresse, int resistance, int lvl, int pa, int pamax, int xp, int xpmax, int lvlmax){
	super(nom,vie,viemax,1,posx,posy,force,adresse,resistance, lvl, lvlmax);
	this.setMana(mana);
	this.setManamax(manamax);
	this.setPa(pa);
	this.setPamax(pamax);
	this.setXp(xp);
}

/*----------------------------------------------- GET/SET -------------------------------------*/


public Inventaire getInv() {
	return inv;
}
public void setInv(Inventaire inv) {
	this.inv = inv;
}
public Armure getArmure() {
	return armure;
}
public void setArmure(Armure armure) {
	this.armure = armure;
}
public Arme getArme() {
	return arme;
}
public void setArme(Arme arme) {
	this.arme = arme;
}
public Objet getO() {
	return o;
}
public void setO(Objet o) {
	this.o = o;
}


public int getXpmax(){
	return this.xpmax;
}

public void setXpmax(int xpmax) {
	this.xpmax = xpmax;
}

public int getMana(){
	return this.mana;
}

public void setMana(int mana){
	this.mana = mana;
}

public int getManamax(){
	return this.manamax;
}

public void setManamax(int manamax){
	this.manamax= manamax;
}

public int getPa(){
	return this.pa;
}

public void setPa(int pa){
	this.pa = pa;
}

public int getPamax(){
	return this.pamax;
}

public void setPamax(int pamax){
	this.pamax = pamax;
}

public int getXp(){
	return this.xp;
}

public void setXp(int xp){
	this.xp= xp;
}



/*----------------------------------------------- METHODES -------------------------------------*/


public void lvlUp() {
		if(this.xp== this.xpmax){
			super.setLvl(super.getLvl() + 1);
			this.xp = 0;
			this.xpmax = 10*this.getLvl();
		}
}


public int getDegats()
{
  return(this.getForce()+this.arme.getImpact()+this.lancerDeDee());
}

public int getAttaque()
{
  return(this.getAdresse()+this.arme.getManiabilite()+this.lancerDeDee());
}

public int getInitiative()
{
  if(this.getResistance()-this.armure.getEncombrement() < 0)
  {
    return 0;
  }
  else
  {
    return(this.getResistance()-this.armure.getEncombrement()+this.lancerDeDee());
  }
}


public int getDefense()
{
	return(this.getResistance()+this.armure.getSolidite()+this.lancerDeDee());
}

public int getEsquive()
{
  if(this.getAdresse()-this.armure.getEncombrement() < 0)
  {
    return 0;
  }
  else
  {
    return(this.getAdresse()-this.armure.getEncombrement()+this.lancerDeDee());
  }
}

public boolean combattre(PNJ p)
{
  int deg = this.getDegats();
  int def = p.getDefense();
  if(deg > def)
  {
    p.setVie(p.getVie()-(deg-def));
			System.out.println(this.getNom()+" inflige "+(deg-def)+" points de d�gats � "+ p.getNom());
			System.out.println("Points de vie de "+this.getNom()+" : "+this.getVie());
			System.out.println("Points de vie de "+p.getNom()+" : "+p.getVie() + "\n(Entrer)");
			@SuppressWarnings("resource")
			Scanner scan = new Scanner(System.in);
			scan.nextLine();
			return true;
  }
  return false;
}



@Override
public String toString() {
	return "Nom = " +getNom() + " mana=" + mana + ", Vie " +getVie() + ", Vie max" + getViemax() + ", manamax=" + manamax + ", pa=" + pa + ", pamax=" + pamax + ", xp=" + xp + ", xpmax="
			+ xpmax;
}

public boolean attaquer(PNJ p)
{
  if(this.getAttaque() > p.getEsquive())
  {
    System.out.println(this.getNom()+" a r�ussi son attaque contre "+p.getNom());
    if(this.combattre(p))
    {
      return true;
    }

  }
  return false;
}

public void MonterPa(){ //monter les points d'actions
	if (this.pa < this.pamax){
		pa++;

	}
}

public void BaisserPa(){ //baisse du nombre de point d'action

	if (this.pa > 0){
		pa--;
	}
}


public boolean prendreObjet(Objet o)
{
  System.out.print(this.inv);
  System.out.println("Voulez vous ajouter cet objet dans votre Inventaire ? ?");
  System.out.println(o);

  char c;
  @SuppressWarnings("resource")
Scanner sc = new Scanner(System.in);
  do
  {
    System.out.print("Entrez o pour oui ou n pour non : ");
    c = sc.next().charAt(0);
  } while (c != 'o' && c != 'n');
  if(c == 'o')
  {
    this.inv.ajouterObjet(o);
    System.out.print(this.inv);
    return true;
  }
  else
  {
    return false;
  }
}

public void utiliserPotionSoin(Soin p)
{
  this.setVie(this.getVie() + p.getHeal());
}









}
