public class Armure extends Objet {

/*------------------------------------------ ATTRIBUTS ------------------------------------------*/
    private int encombrement;
    private int solidite;


/*------------------------------------------ CONSTRUCTEUR ------------------------------------------*/
    public Armure() {}

    public Armure(int encombrement, int solidite) {
    	this.encombrement = encombrement;
    	this.solidite = solidite;
    }
    public Armure(String nom, int typeobjet, int quantite, int encombrement, int solidite) {
        super(nom,typeobjet,quantite);
        this.encombrement = encombrement;
        this.solidite = solidite;
    }

/*------------------------------------------GET/SET ------------------------------------------*/

    public int getSolidite() {
        return this.solidite;
    }
    public void setSolidite(int value) {
        this.solidite = value;
    }
    public int getEncombrement() {
        return this.encombrement;
    }
    public void setEncombrement(int value) {
        this.encombrement = value;
    }


/*------------------------------------------ Methode ------------------------------------------*/
    public String toString() {
        return(this.getNom()+" : \n - Solidite : "+this.solidite+"\n - Encombrement : "+this.encombrement+"\n");
    }

}
