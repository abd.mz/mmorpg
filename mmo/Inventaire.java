import java.util.ArrayList;
import java.util.List;

public class Inventaire {
/*-------------------------------------- ATTRIBUTS --------------------------------------------*/
    private int capmax; // capacite max
    private int nbo;  // nombre d'objet
    private Objet[] contenu;
    public List<Objet> equipement = new ArrayList<Objet> ();


/*-------------------------------------- CONSTRUCTEUR --------------------------------------------*/
   public Inventaire() {}

    public Inventaire(int capmax, int nbo, Objet[] contenu, List<Objet> equipement) {
    	this.capmax = capmax;
    	this.nbo = nbo;
    	this.contenu = contenu;
    	this.equipement = equipement;
    }

/*-------------------------------------- GET/SET --------------------------------------------*/

    public int getCapmax() {
		return capmax;
	}

	public void setCapmax(int capmax) {
		this.capmax = capmax;
	}

	public int getNbo() {
		return nbo;
	}

	public void setNbo(int nbo) {
		this.nbo = nbo;
	}

	public Objet[] getContenu() {
		return contenu;
	}

	public void setContenu(Objet[] contenu) {
		this.contenu = contenu;
	}

	public List<Objet> getEquipement() {
		return equipement;
	}

	public void setEquipement(List<Objet> equipement) {
		this.equipement = equipement;
	}
	public void setNb_Objet(int value) {
        this.nbo = value;
    }
	public Objet getObjet(int i) {
		return this.equipement.get(i);
	}

/*-------------------------------------- METHODE --------------------------------------------*/



    public void ajouterObjet(Objet o) {
        if(this.getNbo() < this.getCapmax())
        {
          this.equipement.add(o);
          this.setNbo(this.getNbo()+1);
        }
      }

      public void jeterObjet(int i)
      {
        if(this.getNbo() > (i))
        {
          this.equipement.remove(i);
          this.setNbo(this.getNbo()-1);
        }
      }


      public String toString()
      {
        String str = "";
        str += "\n********************* \n Voici votre inventaire : \n\n";
        for (int i = 0; i< this.getNbo(); i++)
        {
          str += "Equipement ne"+(i+1)+" "+this.getObjet(i);
        }
        str += ("\n\n\n Vous pouvez encore mettre : "+(this.getCapmax()-this.getNbo())+" objets dans votre Inventaire. \n");
        return str;
      }








}
