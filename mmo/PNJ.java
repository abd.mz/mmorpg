import java.util.Scanner;

public class PNJ extends Personnage{

/*-------------------------------------- Attributs --------------------------------------------*/
	private int nivDifficulte;
	private Objet loot; //lacher objets

/*-------------------------------------- CONSTRUCTEUR --------------------------------------------*/
	public PNJ(){}

	public PNJ(String nom, int vie,int viemax,int typepers, int posx, int posy, int nivDifficulte, int f, int a, int r, Objet loot){
				super.setNom(nom);
				super.setVie(viemax);
				super.setViemax(viemax);
				super.setTypePers(typepers);
				super.setPosx(posx);
				super.setPosy(posy);
				this.setNivDifficulte(nivDifficulte);
				this.setLoot(loot);
			}

/*-------------------------------------- get/set --------------------------------------------*/

	public int getNivDifficulte(){
		return this.nivDifficulte;
	}

	public void setNivDifficulte(int nivDifficulte){
		this.nivDifficulte= nivDifficulte;
	}

	public Objet getLoot(){
		return this.loot;
	}

	public void setLoot(Objet loot){
		this.loot= loot;
	}



/*-------------------------------------- Methode --------------------------------------------*/
	public int getEsquive()
  {
    if(this.getAdresse()-6 < 0)
    {
      return 0;
    }
    else
    {
      return(this.getAdresse()-6+this.lancerDeDee());
    }
  }

  public int getDefense()
  {
  	return(this.getResistance()+this.lancerDeDee());
  }

  public int getInitiative()
  {
    if(this.getResistance()-6 < 0)
    {
      return 0;
    }
    else
    {
      return(this.getResistance()-6+this.lancerDeDee());
    }
  }

  public int getAttaque()
  {
    return(this.getAdresse()+super.lancerDeDee());
  }

  public int getDegats()
  {
    return(this.getForce()+super.lancerDeDee());
  }

	public boolean combattre(PJ p)
	{
		int deg = this.getDegats();
		int def = p.getDefense();
		if(deg > def)
		{
			p.setVie(p.getVie()-(deg-def));
			System.out.println(this.getNom()+" inflige "+(deg-def)+" points de d�gats � "+ p.getNom());
			System.out.println("Points de vie de "+this.getNom()+" : "+this.getVie());
			System.out.println("Points de vie de "+p.getNom()+" : "+p.getVie() + "\n(Entrer)");
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			sc.nextLine();
			return true;
		}
		return false;
	}

	public boolean attaquer(PJ p)
	{
		if(this.getAttaque() > p.getEsquive())
		{
			System.out.println(this.getNom()+" a r�ussi son attaque contre "+p.getNom());
			if(this.combattre(p))
			{
				return true;
			}

		}
		return false;
	}

	public void lacherloot(Object[][] map){
		if(this.estMort()){
			map[this.getPosx()][this.getPosy()] = this. getLoot();
		}
	}



}
