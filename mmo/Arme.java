public abstract class Arme extends Objet {

/*------------------------------------------ ATTRIBUTS ------------------------------------------*/
    private int impact;
    private int maniabilite;
    private int typearme; //1 = Epee, 2 = Sceptre , 3 = Hache
    private int degats;

/*------------------------------------------ CONSTRUCTEUR ------------------------------------------*/

public Arme() {

}

public Arme(String nom,int typearme,  int quantite,  int impact, int maniabilite) {
	super(nom, typearme,quantite);
    this.impact = impact;
    this.maniabilite = maniabilite;
    }

/*------------------------------------------ GET/SET ------------------------------------------*/
public int getImpact() {
	return this.impact;
}
public void setImpact(int value) {
	this.impact = value;
}
public int getManiabilite() {
   return this.maniabilite;
}
public void setManiabilite(int value) {
	this.maniabilite = value;
}
public int getTypearme() {
	return typearme;
}
public void setTypearme(int typearme) {
	this.typearme = typearme;
}
public int getDegats() {
	return degats;
	}

public void setDegats(int degats) {
	this.degats = degats;
}
/*------------------------------------------ METHODE ------------------------------------------*/

public String toString() {
	return "Arme [Impact=" + this.impact + ", Maniabilite=" + this.maniabilite + ", this.typearme=" + this.typearme + ", degats="
				+ degats + "]";
}




}
